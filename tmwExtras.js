const config = require("./src/config");

const https = require("https");
const Internal_to_json = require('xmljson').to_json;

var startTS=Date.now();

var Itemdb1={};
var Itemdb2={};

function httpGet(url){
  return new Promise((resolve,rej) => {
    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        resolve(body);
      });
      //TODO: reject on error
    });
  });
}

function XMLtoJSON(xml){
  return new Promise((resolve,rej) => {
    Internal_to_json(xml, (err,res) => {
      if(err)return rej(err);
      resolve(res);
    });
  });
}

async function parseITEMDB(plainrawdata){
    var rawdata = plainrawdata.split("*******************/")[1];
    rawdata = rawdata.slice(0, rawdata.lastIndexOf(","));
    rawdata = "["+rawdata+"]";
    // remove comments:
    rawdata = rawdata.replace(/\/\/.*/g,"");
    rawdata = rawdata.replace(/\/\*(\n|.)+?\*\//g, "");
    // remove tabs, add commas, remove linebreaks, remove breaking commas
    rawdata = rawdata.replace(/\t/g,"");
    rawdata = rawdata.replace(/(\w*):([^\n<{]*)\n/g,"\"$1\":$2,\n");
    rawdata = rawdata.replace(/\n/g,"");
    rawdata = rawdata.replace(/,\}/g,"}");
    //Trade and scripts and etc.
    rawdata = rawdata.replace(/Trade: \{([^\}]*)\}/g,"\"Trade\": {$1},");
    rawdata = rawdata.replace(/AllowAmmo: \{([^\}]*)\}/g,"\"AllowAmmo\": {$1},");
    rawdata = rawdata.replace(/Nouse: \{([^\}]*)\}/g,"\"Nouse\": {$1},");
    rawdata = rawdata.replace(/AllowCards: \{([^\}]*)\}/g,"\"AllowCards\": {$1},");
    rawdata = rawdata.replace(/(Script|OnEquipScript|OnUnequipScript|OnDropScript|OnTakeScript|OnInsertCardScript): <"(.+?)">/g,function (match,f1,f2){
      var escaped = encodeURI(f2);
      return "\""+f1+"\": \"" + escaped + "\",";
    });

    rawdata = rawdata.replace(/,\}/g,"}");
    var data_array = JSON.parse(rawdata);
    var DB={};
    for (var i = 0; i < data_array.length; i++) {
      var item = JSON.parse(JSON.stringify(data_array[i]));
      delete item.Id;
      DB[data_array[i].Id.toString()] = item;
    }
    return DB;
}

async function parseITEMClientXML(plainrawdata){
  var data = await XMLtoJSON(plainrawdata);
  var items = data.items.item;
  var DB = {};
  for (var key in items) {
    if (items.hasOwnProperty(key) && items[key].$.id >= 0) { // remove hair cuts and races from the list
      var item = items[key];
      DB[item.$.id.toString()]={
        description:item.$.description,
        effect:item.$.effect,
        image:item.$.image.split("|")[0],
        maxFloorOffset:item.$.maxFloorOffset,
        name:item.$.name,
        type:item.$.type,
        weight:item.$.weight
      };
    }
  }
  return DB;
}
function getItemServer(id){return Itemdb1[id];}
function getItemClient(id){return Itemdb2[id];}
function getItem(id){
  var result = {
    client:getItemClient(id),
    server:getItemServer(id)
  };
  //console.log(id,result,Itemdb2[id]);
  return result;
}

var allItems=[];

async function run(){
  var test_item = 513;
  var step1 = await Promise.all([httpGet(config.itemdbUrl), httpGet(config.itemsXML)]);
  console.info("HTTP Requests took "+(Date.now()-startTS).toString()+"ms");
  var step2 = await Promise.all([parseITEMDB(step1[0]), parseITEMClientXML(step1[1])]);
  Itemdb1 = step2[0];
  Itemdb2 = step2[1];

  console.log("Test Itemdb1:", Object.keys(Itemdb1).length, getItemServer(test_item));
  console.log("Test: Itemdb2", Object.keys(Itemdb2).length, getItemClient(test_item));
  for (key in Itemdb1) {
    const item = Itemdb1[key];
    const clientitem = Itemdb2[key];
    allItems.push({
      id:key,
      name:item.Name,
      type:item.Type,
      image:((clientitem && clientitem.image)?clientitem.image:'error.png')
    })
  }
  console.log("Test All Items:", allItems.length, allItems.find((item) => item.id == test_item));
  console.info("Done, Loading took "+(Date.now()-startTS).toString()+"ms");
}


function makeItemDescriptionCards(item_id){
  var item = getItem(item_id);
  var lines = []; // description lines
  for (var i = 0; i < config.discordItemDescription.length; i++) {
    var tmp = config.discordItemDescription[i];
    var val = (tmp[0] || !item.client) ? item.server[tmp[1]] : item.client[tmp[1]];
    if(val)lines.push(tmp[2].replace(/\$val/, val));
  }

  return {
    description: lines.join("\n"),
    title: item.server.Name,
    image:((item.client && item.client.image)?item.client.image:'error.png')
  }
}

run();
var ejs = require('ejs');
var express = require('express');
var app = express();

app.get('/site.css', function(req, res) {
  res.sendFile(__dirname + "/src/site.css");
});
app.get('/json/discord', function(req, res) {
  var ts1 = Date.now();
  var result = {};
  for (key in Itemdb1) {
    result[key] = makeItemDescriptionCards(key);
  }
  console.log("discord generate took: ", Date.now()-ts1, "ms");
  res.send(result);
});
app.get('/', function(req, res) {
  ejs.renderFile("./src/item_list.ejs", {
    categories:config.ItemCategorys,
    data:allItems,
    image_path_prefix:config.image_path_prefix,
    wiki_path:config.wiki_path
  }, {}, function(err, str){
    if(err){
      console.error(err);
      return res.send("500 - There was an error");
    }
    res.send(str);
  });
});
app.get('/json/all', function(req, res) {
  res.send(allItems);
});
app.get('/json/:id', function(req, res) {
  res.send(getItem(req.params.id));
});

function getProperty(item,name){
  var value, name_splited=name.split(".");
  if(name_splited[0] == "server")value = item.server[name_splited[1]];
  else value = item.client[name_splited[1]];
  return {
    internal_name:name_splited[1],
    name:name_splited[1],// Maybe translated
    //description:"[description]",
    value
  }
}

const backbutton = '<br><a href="/">Back to itemlist</a>';// Maybe rather an ejs page in the future?

app.get('/:id', function(req, res) {
  var item = getItem(req.params.id);
  if(typeof(item.client)==="undefined" && typeof(item.server)==="undefined")return res.send("404 - Item with ID "+req.params.id+" not found"+backbutton);
  if(typeof(item.client)==="undefined" || typeof(item.server)==="undefined")return res.send("404 - Item not on both sides found (missing on the client or on the server)<h1 style='color:red'>SAULC YOU BROKE CLIENT DATA AGAIN!!</h1>"+backbutton);

  ejs.renderFile("./src/item_template.ejs", {
    item,
    id:req.params.id,
    image_path_prefix:config.image_path_prefix,
    wiki_path:config.wiki_path,
    tools,
    getProperty:getProperty.bind(null, item),
    properties:config.properties
  }, {}, function(err, str){
    if(err){
      console.error(err);
      return res.send("500 - There was an error"+backbutton);
    }
    res.send(str);
  });
});

app.listen(3000);

var tools={
  decodeScript:(urlencoded)=>{
    return decodeURI(urlencoded).trim().replace(/;/g, ";\n").trim();
  }
}
