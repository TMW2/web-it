# Item View Server - i.tmw2.org

Displays information about items and provides an simple api.

## Usage

Requirements:
- Node 8 LTS required (older versions could work when they have `async` and `await`)
- estimated 17-25 mb RAM (without server load on windows 10 and the item db's from 4/12/2018)

Starting:

1. `npm i` to install all dependencies (only first start and when updating)
2. `node tmwExtras` to start it (if you use it in production you may run it with `forever` or `screen`)

###Endpoints

path | Description | response | format
----|-----|-----|------
`/` | Get a list of all Items (name and icon) | Website | HTML
`/:itemID` | Get the info page from one specific item | Website | HTML
`/site.css` | css for website | css file | CSS
|||
`/json/discord` | Get item preview data for the discord bot | json | ```{'itemid':{ description:string, title:string, image:string },...}```
`/json/all` | The same data that the "all item List"(`/`) uses as json | json | ```{ id:string, name:string, type:string, image:string }[]```
`/json/:itemID`| returns all parsed item info for an item (WARNING no 404 error yet its just a empty json)| json | ```{server:object,client:object}```


## Future Plans and TODO

### TODO: (Somebody works on it)


### Next TODO:
- support dyed items (not only items with color value but also colored items like the metal bars or powders) / Discord bridge have dyed thing other formats
- support items that have other sprites?
  - COntributor Sweater | 1309 has a %color% in the description

- Turn it into a wiki -> add ability to load markdown file from gitlab per ajax with
 additional information like the stuff that is still missing for a good wiki - quests on that you can gain thes and how
  to use them etc. everything that can't be generated automaticaly

### Optional TODO:
- Rarity value (where could we get or calculate that?)
- Community Market Price (where could we get or calculate that? maybe from transaction logs)

- Hover Description of the Properties with language support and /  or language support in general


### Performance Ideas:
- **Client:** generate a css sprite map with all item-icons for better performance in list view?

### Recurring Tasks:
- Bug fixes
- Cleaning the code / simplify thing that are/work better with less lines.
- Finding and solving issues

## Contribute

Please check first whether the thing you want to add/change could be just a `src/config.js` edit away, before you try to code anything.
