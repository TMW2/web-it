//!Important! This cofig file shouldn't contaian sensitive data like passwords or api keys!
module.exports = {
  itemdbUrl : "https://gitlab.com/TMW2/serverdata/raw/master/db/re/item_db.conf",
  itemsXML : "https://gitlab.com/TMW2/clientdata/raw/master/items.xml",
  image_path_prefix : "https://gitlab.com/TMW2/clientdata/raw/master/graphics/items/",
  wiki_path : "https://gitlab.com/TMW2/Docs/wikis/Items",
  discordItemDescription:[
    [false,"description","_$val_"],
    [false,"effect","**$val**"],
    [true,"Atk","> **Attack DMG:** `$val`"],
    [true,"Matk","> **Mana Attack DMG:** `$val`"],
    [true,"Def","> **Defense:** `$val`"],
    [true,"Range","> **Range:** `$val`"],
    [true,"EquipLv","> **Required Level:** `$val`"],
    [true,"Weight","> **Weight:** `$val g`"],
    [true,"Type","> *Type:* `$val`"]
  ],// Templates: [on server:boolean,property,template]
  ItemCategorys:[
    "IT_HEALING",
    "IT_USABLE",
    "IT_DELAYCONSUME",
    "IT_ARMOR",
    "IT_WEAPON",
    "IT_ETC",
  ],//Order for the item categorys on the list page
  properties : { // What should be displayed where on the item view page?
    general:[
      "server.Type",
      "server.Weight",
      "server.Delay",
      "server.Refine",
      "server.DropAnnounce",
      "server.Stack",
      "server.FloorLifeTime"
    ],
    specific:[
      "server.Atk",
      "server.Def",
      "server.Matk",
      "server.Range",
      "server.MinRange",
      "server.WeaponLv",
      "server.EquipLv"
    ],
    expert:[
      "server.AegisName",
      "server.Subtype",
      "server.ViewSprite",
      "server.Slots",
      "server.Job",
      "server.Upper",
      "server.Gender",
      "server.DisableOptions",
      "server.BindOnEquip",
      "server.ForceSerial",
      "server.KeepAfterUse",
      "server.BuyingStore",
      "server.AllowPickup"
    ]
  }
};
